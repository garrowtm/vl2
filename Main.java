import java.util.Scanner;
import java.lang.Math;

public class Main {
    public static void main(String[] args)
    {
        Scanner scan = new Scanner(System.in);

        System.out.println("Введите температуру воздуха (в градусах)");
        int temp = scan.nextInt();

        System.out.println("Введите скорость ветра (в метрах в секунду)");
        int wind_speed = scan.nextInt();

        /*System.out.println("Введите направление ветра (n, s, w, e) ");
        char wind_dir = scan.next().charAt(0);        [не учитвается] */

        System.out.println("Есть ли дождь? (y - да, n - нет)");
        char is_raining = scan.next().charAt(0);

        if (is_raining == 'y')
        {
            System.out.println("Лучше не выходить, промокнете");
        }
        else
        {
            double real_temp = 13.12 + 0.6215 * temp - 11.37 * Math.pow(wind_speed,0.16) + 0.3965 * temp * Math.pow(wind_speed,0.16);
            if (real_temp < 10)
            {
                System.out.println("Лучше не выходить, замерзнете");
            }
            else if (real_temp > 40)
            {
                System.out.println("Лучше не выходить, задохнетесь");
            }
            else
            {
                System.out.println("Приятной прогулки");
            }
        }
    }
}